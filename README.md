Contents
========
 - Building.
 - Installing.
 - Using.



Building
========
There are several ways to build project:
 - build using NAnt (recomended);
 - build using MonoDevelop IDE;
 - build using terminal and compiler.

#### Building using NAnt
If you deside build project using NAnt, you have call nant from project root directory, like this:
`$ nant`
Additionaly you can call NAnt with parameters:
 - for remove artifacts: `$ nant clean`
 - for building: `$ nant build`
 - for check source cod using Gendarme: `$ nant gendarme`

#### Building using MonoDevelop
If you deside build project using MonoDevelop, you have open project in MonoDevelop and launch "Build" from it.

#### Building using bare compiler
If you deside build project using terminal and compiler you must know how do it.



Installing
==========
For deploy project you must install several programs and configure their.
On server side you must install:

For OpenSUSE:
- apache2;
- apache2-mod_mono.

For Debian:
- apache2;
- mono-apache-server;
- libapache-mod-mono.

For enable mod_mono run this command:
`# a2enmod mod_mono`

Create project config file: /etc/apache2/conf.d/WebTerminal with content like this:
```
Alias / "home/user/public_html"
MonoServerPath WebTerminal "/usr/bin/mod-mono-server2"
MonoSetEnv WebTerminal MONO_IOMAP=all
MonoApplications WebTerminal "/:/home/user/public_html"
<Location "/">
    Allow from all
    Order allow,deny
    MonoSetServerAlias WebTerminal
    SetHandler mono
    SetOutputFilter DEFLATE
    SetEnvIfNoCase Request_URI "\.(?:gif|jpe?g|png)$" no-gzip dont-vary
</Location>
```

Edit mod_mono config file: /etc/apache2/conf.d/mod_mono.conf like this (for i386 system change /usr/lib64/apache2/mod_mono.so
on /usr/lib/apache2/modules/mod_mono.so):
```
<IfModule !mod_mono.c>
    LoadModule mono_module /usr/lib64/apache2/mod_mono.so
</IfModule>

MonoAutoApplication disabled
AddHandler mono .aspx .ascx .asax .ashx .config .cs .asmx .axd
MonoApplications "/:/home/user/public_html"

AddType application/x-asp-net .aspx
AddType application/x-asp-net .asmx
AddType application/x-asp-net .ashx
AddType application/x-asp-net .asax
AddType application/x-asp-net .ascx
AddType application/x-asp-net .soap
AddType application/x-asp-net .rem
AddType application/x-asp-net .axd
AddType application/x-asp-net .cs
AddType application/x-asp-net .vb
AddType application/x-asp-net .master
AddType application/x-asp-net .sitemap
AddType application/x-asp-net .resources
AddType application/x-asp-net .skin
AddType application/x-asp-net .browser
AddType application/x-asp-net .webinfo
AddType application/x-asp-net .resx
AddType application/x-asp-net .licx
AddType application/x-asp-net .csproj
AddType application/x-asp-net .vbproj
AddType application/x-asp-net .config
AddType application/x-asp-net .Config
AddType application/x-asp-net .dll
DirectoryIndex index.aspx
DirectoryIndex Default.aspx
DirectoryIndex default.aspx
```



Using
=====
Just type in address fild of youre favorite browser: `http://server-name:80/Default.aspx`.

By default apache use port 80, so you have not specify port: `http://server-name/Default.aspx`.

Additionaly you have not specify default page:`http://server-name`.

![Screenshot](screenshot.png)
