using System;

namespace ShellProxy
{
    public class ShellProxy
    {
        public ShellProxy ()
        {
        }

        public String Execute (String command, String path, String parameters, out int exitCode)
        {
            System.Diagnostics.ProcessStartInfo processInfo =
                new System.Diagnostics.ProcessStartInfo (path + command, parameters);
            processInfo.RedirectStandardOutput = true;
            processInfo.UseShellExecute = false;

            System.Diagnostics.Process process = System.Diagnostics.Process.Start (processInfo);
            string toolOutput = process.StandardOutput.ReadToEnd ();
            process.WaitForExit ();
            exitCode = process.ExitCode;
            return toolOutput;
        }
    }
}
