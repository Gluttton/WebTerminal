<%@ Page Language="C#" Inherits="WebTerminal.Default" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head runat="server">
	<title>Web terminal</title>
	<link rel="stylesheet" href="~/App_Themes/color.css" type="text/css" />
	<link rel="stylesheet" href="~/App_Themes/style.css" type="text/css" />
</head>
<body class="yui-skin-sam">
	<form id="formMain" runat="server">
		<div id="mainMain" align="center">
			<table id="tableWidthWrapper" width="90%"><tr><td>
				<div id="divHeader" align="center" style="background-color: #eeeeee; color: #000000">
					<h2><asp:Label id=labelHeader runat="server" Text="Web access control center"/></h2>
				</div>
				<br/>
				<table id="tableMain" style="margin-top:0px; border-top:none;" class="sortable pane bigtable">
					<caption><h3>Execute any command</h3></caption>
					<tbody>
						<tr></tr>
						<tr style="border-top:0px;"> <th width="200px">Parameter</th> <th width="600px">Value</th> </tr>
						<tr>
							<td><asp:Label id=labelCommandTitle runat="server" Text="Command"/> </td>
							<td><asp:TextBox id=textBoxCommand Width="100%" runat="server" text="bash"/></td>
						</tr>
						<tr>
							<td><asp:Label id=labelPathTitle runat="server" Text="Path"/></td>
							<td><asp:TextBox id=textBoxPath Width="100%" runat="server" text="/bin/"/></td>
						</tr>
						<tr>
							<td><asp:Label id=labelParametersTitle runat="server" Text="Parameters"/></td>
							<td><asp:TextBox id=textBoxParameters Width="100%" runat="server" text="-c &quot; &quot;"/></td>
						</tr>
						<tr>
							<td><asp:Label id=labelExitCodeTitle runat="server" Text="Exit code"/></td>
							<td><asp:Label id=labelExitCode runat="server"/></td>
						</tr>
						<tr>
							<td><asp:Label id=labelOutputTitle runat="server" Text="Result output"/></td>
							<td><asp:Label id=labelOutput Width="100%" Height="150px" TextMode="MultiLine" runat="server"/></td>
						</tr>
					</tbody>
				</table>
				<br/>
				<div id="divExecute" align="center">
					<asp:Button id="buttonExecute" runat="server" Text="Execute" OnClick="buttonExecuteClicked"/>
				</div>
				<br/>
				<div id="divFooter" align="right" style="background-color: #eeeeee; color: #000000">
					<asp:Label id=labelLogo runat="server"/>
				</div>
			</td></tr></table>
		</div>
	</form>
</body>
</html>
