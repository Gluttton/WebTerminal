using System;
using System.Web;
using System.Web.UI;

namespace WebTerminal
{
	public partial class Default : System.Web.UI.Page
	{
		protected void Page_Load (object sender, EventArgs e)
		{
			DateTime now = DateTime.Now;
			labelLogo.Text = now.ToString ("dd-MM-yyyy HH:mm:ss");
		}

		public virtual void buttonExecuteClicked (object sender, EventArgs args)
		{
			ShellProxy.ShellProxy shell = new ShellProxy.ShellProxy ();
			string command = textBoxCommand.Text;
			string path = textBoxPath.Text;
			string parameters = textBoxParameters.Text;
			int exitCode = 0;
			string output = string.Empty;
			try {
				output = shell.Execute (command, path, parameters, out exitCode);
			}
			catch {
				output = "Error occurred during execute command!";
				exitCode = -1;
			}
			labelExitCode.Text = exitCode.ToString ();
			labelOutput.Text = output.Replace (Environment.NewLine, "<br/>");
		}
	}
}